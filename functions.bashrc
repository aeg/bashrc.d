# function checks if the application is installed
function __add_command_replace_alias() {
    if [ -x "$(which $2 2>&1)" ]; then
        alias ${1}=${2}
    fi
}
__add_command_replace_alias mtail 'multitail'
__add_command_replace_alias df 'pydf'
__add_command_replace_alias traceroute 'mtr'
__add_command_replace_alias tracepath 'mtr'
__add_command_replace_alias top 'htop'
__add_command_replace_alias vi 'vim'
__add_command_replace_alias less 'most'
__add_command_replace_alias more 'most'


# Extract anything
extract () {
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)  tar xjf $1    ;;
      *.tar.gz) tar xzf $1    ;;
      *.tar.xz) tar xJf $1    ;;
      *.bz2)    bunzip2 $1    ;;
      *.rar)    rar x $1    ;;
      *.gz)   gunzip $1   ;;
      *.tar)    tar xf $1   ;;
      *.tbz2)   tar xjf $1    ;;
      *.tgz)    tar xzf $1    ;;
      *.zip)    unzip $1    ;;
      *.Z)    uncompress $1 ;;
      *.7z)    p7zip -d $1 ;;
      *)      unp $1 || echo "'$1' cannot be extracted via extract()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# Clock
clock() {
  watch -t -n1 "echo "" && date +""$'  '"%T""
}

# read cert
readcert() {
  openssl x509 -in "${1}" -text -noout |more
}

readcsr() {
  openssl req -noout -text -in $1 |more
}

# read cert
getcert() {
  openssl s_client -connect "${@}" |more
}

# List files installed by package
dpkg-files() {
  dpkg-query -L "${1}"
}

# List package dep
dpkg-dep() {
  apt-rdepends "${@}"
}

# Find recently edited files
rf() {
  find ${1} -type f -printf '%TY-%Tm-%Td %TT %p\n' | sort -r
}

# Compress into cbz
if [[ -r /usr/bin/zip ]]; then
  cbz() {
    zip -r ${1}.cbz ${1}
  }
fi

# Replace spaces by _
replace() {
  rename "y/ /${2:-_}/" "${1}"
}

# Replace space by tab
replace-space-tab() {
  sed -i 's/ /\t/g' "${1}"
}

# Replace tab by space
replace-tab-space() {
  sed -i 's/\t/ /g' "${1}"
}

# Get temperature (raspberrypi)
if [[ -r /usr/bin/vcgencmd ]] || [[ -r /opt/vc/bin/vcgencmd ]] ; then
  temp() {
    vcgencmd measure_temp|awk -F'=' '{print $2}'
  }
fi

# FAIL2BAN
if [ -x "/usr/bin/fail2ban-client" ] ; then
  if [ "$UID" -eq "0" ] ; then
    # list jails status
    fail2ban-jails() {
        for JAIL in $(fail2ban-client status | grep 'Jail list' | sed -E 's/^[^:]+:[ \t]+//' | sed 's/,//g') ;
            do fail2ban-client status "${JAIL}" ;
        done
    }

    fail2ban-jail() {
      fail2ban-client "${@}"
    }
    # unban ip from fail2ban, first argument is jail, second is ip
    fail2ban-unban() {
      fail2ban-client set "${1}" unbanip "${2}"
    }
  fi
fi

# if the command-not-found package is installed, use it
if [ -x /usr/lib/command-not-found -o -x /usr/share/command-not-found/command-not-found ]; then
        function command_not_found_handle {
                # check because c-n-f could've been removed in the meantime
                if [ -x /usr/lib/command-not-found ]; then
                   /usr/lib/command-not-found -- "$1"
                   return $?
                elif [ -x /usr/share/command-not-found/command-not-found ]; then
                   /usr/share/command-not-found/command-not-found -- "$1"
                   return $?
                else
                   printf "%s: command not found\n" "$1" >&2
                   return 127
                fi
        }
fi

# bat + git diff
batdiff() {
    git diff --name-only --diff-filter=d | xargs bat --diff
}

# Git commit & push
gcp() {
  git commit -a -m "$@" || echo "Commit failed" ;
  git push || echo "Push failed" ;
}

#------------------#
# Docker Functions #
#------------------#

# Get docker container ip
dockerip() {
  docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}} {{end}}' "${1}"
}

# Open sh terminal in container
dbash() {
  docker exec -it "${1}" bash || docker exec -it "${1}" sh
}

# Print listening ports in container
dlisten() {
  dockerPID=$(docker inspect -f '{{.State.Pid}}' ${1})
  nsenter -t "${dockerPID}" -n netstat -ntlp
}

# stop and rm
dsrm() {
  docker stop "${@}" && docker rm "${@}"
}
