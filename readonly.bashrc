# Used to know current filesystem mode (rw/ro)
fs_mode=$(mount | sed -n -e "s/^.* on \/ .*(\(r[w|o]\).*/\1/p")
if [ "$(grep ' / ' /etc/fstab |grep ro|grep -v '\-ro')" ]; then

  if [ "$(command -v logread 2>/dev/null)" ]; then
    alias syslog='logread -f'
  fi
  HISTFILE="/tmp/${USER}_bash_history"

  # alias ro/rw pour passer de l'un à l'autre
  alias fsro='mount -o remount,ro / ; fs_mode=$(mount | sed -n -e "s/^.* on \/ .*(\(r[w|o]\).*/\1/p")'
  alias fsrw='mount -o remount,rw / ; fs_mode=$(mount | sed -n -e "s/^.* on \/ .*(\(r[w|o]\).*/\1/p")'
  if [[ -z ${temp+x} ]]; then
    PROMPT_COMMAND='PS1="[\[$(tput sgr0)\]\[\033[38;5;214m\]${fs_mode}\[$(tput sgr0)\]\[\033[38;5;15m\]]\[$(tput sgr0)\]\[\033[38;5;11m\]\u\[$(tput sgr0)\]\[\033[38;5;15m\]@\h:\[$(tput sgr0)\]\[\033[38;5;6m\][\w]:\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"'
  else
    PROMPT_COMMAND='PS1="[\[$(tput sgr0)\]\[\033[38;5;214m\]${fs_mode}|$(temp)\[$(tput sgr0)\]\[\033[38;5;15m\]]\[$(tput sgr0)\]\[\033[38;5;11m\]\u\[$(tput sgr0)\]\[\033[38;5;15m\]@\h:\[$(tput sgr0)\]\[\033[38;5;6m\][\w]:\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"'
  fi
fi
