## SSH-Agent
SSH_ENV="$HOME/.ssh/environment"
function start_agent {
    echo "Initialising new SSH agent..."
    /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
    echo succeeded
    chmod 600 "${SSH_ENV}"
    source "${SSH_ENV}" > /dev/null
    /usr/bin/ssh-add;
}
# Source SSH settings, if applicable
if [ -f "${SSH_ENV}" ]; then
    source "${SSH_ENV}" > /dev/null
    ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
      start_agent;
    }
else
    start_agent;
fi

# Run ssh sessions with tmux
if [ -x "/usr/bin/tmux" ] ; then
    if [ -z "${TMUX}" ]; then
        function ssht () {
            tmux attach "${1}" || tmux new -A -s "${1}" "ssh ${1}"
        }
    fi
fi
