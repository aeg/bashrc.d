HISTSIZE=100000
HISTFILESIZE=${HISTSIZE}
HISTCONTROL=ignoredups
HISTTIMEFORMAT="$(tput setaf 6) %d/%m/%Y %H:%M:%S $(tput setaf 7) "
PROMPT_COMMAND="history -a"
