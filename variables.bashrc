# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Editor
export SUDO_EDITOR=vim
export EDITOR=vim
export VISUAL=vim
if [ "$(command -v most 2>/dev/null)" ]; then
  export PAGER="most -s"
  export MANPAGER="most -s"
else
  export PAGER="less -s"
  export MANPAGER="less -s"
fi

# GPG
export GPG_TTY=$(tty)

# Used for ls aliases
export LS_OPTIONS='--color=auto --group-directories-first'

# Tip to show a reboot if required
if [ -f /run/reboot-required ]; then
  REBOOT=' [!]'
fi

# Add gitaware compliant
if type __git_ps1 &>/dev/null ; then
  export GIT_PS1_SHOWDIRTYSTATE=true
  export GIT_PS1_SHOWUNTRACKEDFILES=true
  export GIT_PS1_SHOWSTASHSTATE=true
  export GIT_PS1_SHOWCOLORHINTS=true
  export GIT_PS1_SHOWUPSTREAM="verbose"
  GIT_BRANCH="\`__git_ps1\`"
fi

# set a fancy prompt (non-color, overwrite the one in /etc/profile)
BLACK="\[$(tput setaf 0)\]"
RED="\[$(tput setaf 1)\]"
GREEN="\[$(tput setaf 2)\]"
ORANGE="\[$(tput setaf 3)\]"
BLUE="\[$(tput setaf 4)\]"
MAGENTA="\[$(tput setaf 5)\]"
CYAN="\[$(tput setaf 6)\]"
WHITE="\[$(tput setaf 7)\]"
YELLOW="\[$(tput setaf 11)\]"
PS1="${YELLOW}\u${WHITE}@\h:${CYAN}\w${BLUE}${BOLD}${GIT_BRANCH}${RED}${REBOOT}${WHITE} \\$ "
export PS1

# Bat
if [ "$(command -v bat 2>/dev/null)" ]; then
  export MANPAGER="sh -c 'col -bx | bat -l man -p'"
  export BAT_THEME=ansi
  export BAT_PAGER=less
fi
