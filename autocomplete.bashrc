# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

## Autocompletion for remote connections
_complete_hosts () {
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    host_list=`{
        for c in /etc/ssh_config /etc/ssh/ssh_config ~/.ssh/config
        do [ -r $c ] && sed -n -e 's/^Host[[:space:]]//p' -e 's/^[[:space:]]*HostName[[:space:]]//p' $c
        done
        for k in /etc/ssh_known_hosts /etc/ssh/ssh_known_hosts ~/.ssh/known_hosts
        do [ -r $k ] && egrep -v '^[#\[]' $k|cut -f 1 -d ' '|sed -e 's/[,:].*//g'|grep -v '|1|'
        done
        sed -n -e 's/^[0-9][0-9\.]*//p' /etc/hosts; }|tr ' ' '\n'|grep -v '*'`
    COMPREPLY=( $(compgen -W "${host_list}" -- $cur))
    return 0
}
complete -F _complete_hosts ssh
complete -F _complete_hosts ssht
complete -F _complete_hosts host
complete -F _complete_hosts telnet
complete -F _complete_hosts ping

## scaleway-cli
_scw() {
        _get_comp_words_by_ref -n = cword words
        output=$(scw autocomplete complete bash -- "$COMP_LINE" "$cword" "${words[@]}")
        COMPREPLY=($output)
        # apply compopt option and ignore failure for older bash versions
        [[ $COMPREPLY == *= ]] && compopt -o nospace 2> /dev/null || true
        return
}
complete -F _scw scw

## Pipenv
if [ "$(command -v pipenv 2>/dev/null)" ]; then
  eval "$(_PIPENV_COMPLETE=bash_source pipenv)"
fi

# Git auto-completion
if [ -f /usr/share/git-core/git-prompt.sh ]; then
  source /usr/share/git-core/git-prompt.sh
fi
