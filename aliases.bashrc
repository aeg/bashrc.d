## Aliases

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
  test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
  alias ls='ls --color=auto'
  alias dir='dir --color=auto'
  alias vdir='vdir --color=auto'
  alias grep='grep --color=auto'
  alias fgrep='fgrep --color=auto'
  alias egrep='egrep --color=auto'
fi

# cd
alias cd..='cd ..'
alias ..='cd ..'
alias ...='cd ../../'
alias ....='cd ../../../'
alias .....='cd ../../../../'

# docker
alias dstop-all='docker stop $(docker ps -a -q)'
alias dstart-all='docker start $(docker ps -a -q)'
alias drm-all='docker rm $(docker ps -a -q)'
alias drme='docker rm $(docker ps -a -f status=exited -q)'
alias dps="docker ps "
alias di="docker images "
alias drmi="docker rmi "
alias drm="docker rm "
alias dclean="docker system prune -a"
alias dvclean="docker volume prune"

# git
alias gs='git status'
alias ga='git add'
alias gaa='git add --all'
alias gcm='git checkout master'
alias gcn!='git commit --no-edit --amend'
alias gp='git push'
alias gpf='git push -f'
alias gpull='git pull'
alias gd='git diff'
alias gco='git checkout'
alias gc='git commit -m'
alias ungit="find . -name '.git' -exec rm -rf {} \;"
alias gbpurge="git branch -vv | grep ': gone]'|  grep -v \"\*\" | awk '{ print $1; }' | xargs -r git branch -d"
alias gpsm='git submodule update --recursive --remote'

# ls
alias sl='ls'
alias la='ls $LS_OPTIONS -lh'
alias ll='ls $LS_OPTIONS -alFh'
alias lsa='ls $LS_OPTIONS -alFh'
alias l='ls $LS_OPTIONS -CF'
alias lx='ls $LS_OPTIONS -lXB'  # Sort by extension.
alias lk='ls $LS_OPTIONS -lSr'  # Sort by size, biggest last.
alias lt='ls $LS_OPTIONS -ltr'  # Sort by date, most recent last.
alias lc='ls $LS_OPTIONS -ltcr' # Sort by/show change time,most recent last.
alias lu='ls $LS_OPTIONS -ltur' # Sort by/show access time,most recent last.

#--------------------#
## DEFAULTS OPTIONS ##
#--------------------#

alias mkdir='mkdir -pv'
alias mv='mv -iv'
alias cp='cp -v'
alias ipa='ip -4 a'
alias ip6a='ip -6 a'
alias df='df -h'
alias du='du -hs'
alias wget='wget -c '
alias iotop='iotop -a -o '
alias tailf='tail -f'

#-----------------#
## MISCALLENIOUS ##
#-----------------#

# Debug /dev access
alias devdebug='script /dev/null'

# show paths
alias path='echo -e ${PATH//:/\\n}'

# print actual hour (hour:min:sec)
alias hour='date +"%T"'

# print actual day (day-month-year)
alias day='date +"%d-%m-%Y"'

# update on one command
alias update='sudo apt-get update && sudo apt-get dist-upgrade'

# pass generation
alias newpass="openssl rand -base64 20"
alias newpassha='python -c "from passlib.hash import sha512_crypt; import getpass; print sha512_crypt.encrypt(getpass.getpass())"'

# get my public ip
alias myip='curl ipinfo.io/ip'

# get my privates ips
alias myips="ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p'"

# Show the top 10 cpu using threads, sorted numerically
alias tcpu='ps -eo pcpu,pid,user,args | sort -k1 -r -n | head -10'

# Show deleted files but still there, kept by some process
alias lsofd="lsof -nP | grep -i deleted"

# check fpm sockets
alias fpmsock='ss -x state listening | grep php'

# List iptables rules
alias iptablesL='iptables -L --line-numbers -n '
alias ip6tablesL='iptables -L --line-numbers -n '

# Update all git repositories
alias gpullall='for i in */.git; do cd $(dirname $i); git pull; cd ..; done'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

alias sudo='sudo ' # thanks to that, sudo will recognize alias commands
alias cx="chmod +x"
alias :q='exit'
alias hs='history | grep '

if [ "$(command -v trash 2>/dev/null)" ]; then
  alias rm=trash
else
  alias rm='rm -Iv --one-file-system --preserve-root'
fi

if [ "$(command -v smem 2>/dev/null)" ]; then
  alias memswap='smem -u -s swap -p'
fi
